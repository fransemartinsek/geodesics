function grad = ellipsoidgradf(X)
  %geodesic([0;0;2],[1;0;0],0,5,0.01,'ellipsoid')
  a = 0.5;
  b = 1;
  c = 2;
  grad = [2*X(1)/(a^2); 2*X(2)/(b^2); 2*X(3)/(c^2)];
endfunction