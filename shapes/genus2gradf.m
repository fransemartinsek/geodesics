function g = genus2gradf(X)
  %geodesic([1;1;1/sqrt(3)],[1;0;0],0,5,0.01,'genus2')
  g = zeros(3,1);
	x = X(1);
  y = X(2);
  z = X(3);
  g(1) = 4*(x^3 + x * y * (y + 3 * z^2 - 3));
  g(2) = x^2 * (4 * y + 6 * z^2 - 6) + 2 * y^2 * (2*y - 3 * z^2 + 3);
  g(3) = 4 * z * (3 * x^2 * y - y^3 + 9 * z^2 - 5);
end