function grad = hparaboloidgradf(X)
  %geodesic([1;1;0],[1;0;0], 0, 6, 0.01,'hparaboloid')
	a = 2;
	b = 2;
	grad=[2*X(1)/a^2; -2*X(2)/b^2; 1];
end