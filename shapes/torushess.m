function H = torushess(X)
  R = 3;
  x = X(1);
  y = X(2);
  H = zeros(3,3);
  H(1,1) = 2 - (2*R*y^2)/(x^2 + y^2)^(3/2);
  H(1,2) = (2*R*x*y)/(x^2 + y^2)^(3/2);
  H(2,1) = (2*R*x*y)/(x^2 + y^2)^(3/2);
  H(2,2) = 2 + (2*R* y^2)/(x^2 + y^2).^(3/2) - (2*R)/sqrt(x^2 + y^2);
  H(3,3) = 2;
endfunction
