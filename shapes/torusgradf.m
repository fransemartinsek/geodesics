function g = torusgradf(X)
  %geodesic([3;0;1],[1;0;0],0,3.2,0.01,'torus')
  R = 3;
  g = zeros(3,1);
 g(1) = X(1)*(2- (2*R)/sqrt(X(1).^2 + X(2).^2));
 g(2) = X(2)*(2- (2*R)/sqrt(X(1).^2 + X(2).^2));
 g(3) = X(3)*2;
endfunction
