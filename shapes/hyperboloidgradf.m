function grad = hyperboloidgradf(X)
  %geodesic([0;0;2],[1;0;0],0,4,0.01,'hyperboloid')
  %geodesic([0;0;-2],[1;0;0],0,4,0.01,'hyperboloid')
	a = 2;
	b = 2;
  c = 3;
	grad=[2*X(1)/a^2; 2*X(2)/b^2; -2*X(3)/c^2];
end