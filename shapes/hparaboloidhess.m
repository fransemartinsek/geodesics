function H = hparaboloidhess(X)
	a = 2;
	b = 2;
	H(1,:) = [2/a^2; 0; 0];
	H(2,:) = [0; -2/b^2; 0];
	H(3,:) = [0; 0; 0];
end