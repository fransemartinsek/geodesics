function grad = spheregradf(X)
  %geodesic([1;1;sqrt(2)],[1;0;1],0,2*pi,0.01,'sphere')%tilted
  %geodesic([0;0;2],[1;0;0],0,2*pi,0.01,'sphere')
  grad = [2*X(1); 2*X(2); 2*X(3)];
endfunction