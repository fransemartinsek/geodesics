function H = ellipsoidhess(X)
  a = 0.5;
  b = 1;
  c = 2;
  H = [2/(a^2),0,0;0,2/(b^2),0;0,0,2/(c^2)];
endfunction