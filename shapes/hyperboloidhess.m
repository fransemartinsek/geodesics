function H = hyperboloidhess(X)
  a = 2;
  b = 2;
  c = 3;
  H = [2/(a^2),0,0;0,2/(b^2),0;0,0,-2/(c^2)];
endfunction