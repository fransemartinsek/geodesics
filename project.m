function v = project(T,X,gradf)
    %function to project vector X to tangent plain defined with gradf at point T
    n = feval(gradf,T); %calculate the normal vector
    n = n/norm(n); %normalize the vector
    P = eye(3)-n*n';%crate the P projection matrix
    v = P*X; %project X to the surface
    v = v/norm(v); %normalize the vector
endfunction
