function y = rk4(y0, n, h, gradf, hess) 
  %y0 - [starting point; tangent vector]
  %n - num of points to be found
  %h - length of a single step
  %gradf,hess - name of function to calculate gradient and hessian at a given point

  y = zeros(length(y0), n); %allocate space for n points
  y(:,1) = y0; %set first point 
  for i=1:n-1 %calclating n-1 points with 4th ord. Runge Kutta 
    k1 = h*equat( y(:,i) ,gradf,hess);
    k2 = h*equat( y(:,i)+k1/2 ,gradf,hess);
    k3 = h*equat( y(:,i)+k2/2 ,gradf,hess);
    k4 = h*equat( y(:,i)+k3 ,gradf,hess);
    
    %calculating next point approximation
    y(:, i+1) = y(:, i) + (k1+ 2*k2 + 2*k3 + k4)/6; 
  end
endfunction
 
%function to calculate the tangent vector and acceleration in point x
function rez = equat(y,gradf,hess) 
  x = y(1:3);
  v = y(4:6);
  grad = feval(gradf,x); %gradient in point x
  ngrad = grad'*grad; %||grad||^2
  a = -(grad/ngrad) * v' * feval(hess,x) * v;% acceleration vector in point x in direction v 
  rez = [v; a];
endfunction


