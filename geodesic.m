function geodesic(x0,v0,t0,t1,h,object,num_directions=101,circles=0)
  %x0 - starting point
  %v0 - tangent vector at x0
  %t0,t1 - start and end of interval on which we will calculate points
  %h - length of a single step
  %object - name of surface 
  %cirles - flag to set display of perpendicular circles
  %num_directions - number of directions to calculate geodesics in
  
  num_points = ceil((t1 - t0)/h); %num of points to be caluculated for each geodesic
  grad = [object,'gradf']; %name of gradient function for object
  hess = [object,'hess']; %name of hessian function for object
  addpath ("./shapes"); 
 
  if (circles) %initializations of storage for geodesics
    X = zeros(num_points,num_directions);
    Y = zeros(num_points,num_directions);
    Z = zeros(num_points,num_directions);
  endif
  
  n = feval(grad,x0); %calculate normal vector in point x0
  v0 = project(x0,v0,grad); %projecting v0 to surface in case it wasnt tangential
  z = cross(v0,n)/norm(n); %normalized cross product of v0 and n
  theta = linspace(0, 2*pi, num_directions);
  
  for i=1:num_directions %calculating num_direction geodesics 
    v = cos(theta(i))*v0 + sin(theta(i))*z; %rotating v0 around n by theta(i) radians  
    y = [x0; v];
    y = rk4(y, num_points, h, grad, hess); %callculate one geodesic
    
    %for the circles
    if (circles)
      X(:,i) = y(1,:);
      Y(:,i) = y(2,:);
      Z(:,i) = y(3,:);
    endif

    plot3(y(1,:), y(2,:), y(3,:)); %plot geodesic on figure
    hold on
  end

  %for the circles
  if (circles)
    %surf(X,Y,Z);
    for i=1:15:num_points
      plot3(X(i,:), Y(i,:), Z(i,:));
    end
  endif
  axis equal; 
endfunction
%!test
%! object = 'sphere';
%! x0 = [0;0;2];
%! v0 = [1;0;0];
%! h = 0.1;
%! t0 = 0;
%! t1 = 2*pi;
%! geodesic(x0,v0,t0,t1,h,object);